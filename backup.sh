#!/bin/bash

# Cur usr home dir
dir_to_bckup_home="$HOME/backup"

# Normal backup dir name as per the assignment necessity
cb_d="${dir_to_bckup_home}/cb"

# Inc. backup dir name as per the instructions of the assignment
ib_d="${dir_to_bckup_home}/ib" # Directory path for incremental backup


if [ ! -d "${dir_to_bckup_home}" ]; then
    # Dir is not there, create it
    mkdir -p "${dir_to_bckup_home}"
    # This arg avoids err even if the dir is there
fi


# add of log fl
bc_lg_f="${dir_to_bckup_home}/backup.log"

# Def of fn that is dedicated for normal backups
cr_cmp_b() {
    # cur t when the complete backup is done
    cb_ts=$(date +"%Y%m%d_%H%M%S")
    
    # below cmnd makes unique .tar fname based on prev cnts
    cmp_bc_f="$(printf "${cb_d}/cb%05d.tar" "$cb_cnt")"
    # A var to store prev cnt of cmp bckup
    lst_cb_cnt="$(printf "%05d" "$cb_cnt")"
    # inc cnt
    cb_cnt=$((cb_cnt+1))

    # cur t of backup
    cb_lg_ts=$(date +"%a %d %b %Y %I:%M:%S %p %Z")

    # Below cmnd rets all the files found at path after $HOME
    find "$HOME" -type f -name "*.txt" -not -path "*/\.*" -exec tar -C "$HOME" -cf "$cmp_bc_f" {} +

    echo "Built ${cmp_bc_f} on ${cb_lg_ts}"
    
    # Below text is a requirement of the assignment. Needed to log the file in the below format only
    echo "${cb_lg_ts} $(basename "${cmp_bc_f}") was created" >> "${bc_lg_f}"
}

# Def of fn that is dedicated for inc backups
cr_incre_b() {
    # cur t for inc bck
    inc_bc_ts=$(date +"%Y%m%d_%H%M%S")

    # is it first encounter?
    if [ "$lst_ib_ts" == "" ]; then
    	# Yes First enc for inc bck
        fl_f_inc_bc=$(find "$HOME" -type f -name "*.txt" -newer "${cb_d}/cb${lst_cb_cnt}.tar")
    	# get files from prev cmp bck
    	
    else
        # not the first loop for inc bck
        local lcl_ib_cnt="$(printf "%05d" "$ib_cnt")"
        fl_f_inc_bc=$(find "$HOME" -type f -name "*.txt" -newer "${ib_d}/ib${lcl_ib_cnt}.tar")
        # get fls from prev inc bck
    fi
    
    # log t stmp
    inc_bc_lg_ts=$(date +"%a %d %b %Y %I:%M:%S %p %Z")
    
    # is there any fls with mods or fls that are created after last bck
    if [ -n "$fl_f_inc_bc" ]; then
        # manage cnts
    	ib_cnt=$((ib_cnt+1))
    	inc_bc_fl="$(printf "${ib_d}/ib%05d.tar" "${ib_cnt}")"
    	
    	# zip them accordingly
        tar -C "$HOME" -cf "${inc_bc_fl}" ${fl_f_inc_bc} 2>/dev/null
	# make entry in the lg fl
        echo "${inc_bc_lg_ts} $(basename "${inc_bc_fl}") was created" >> "${bc_lg_f}"

        # duplicate the cnt
        lst_ib_ts=$inc_bc_ts
    else
        # make entry iun the lg fl as per the requirement of the assignment
        echo "${inc_bc_lg_ts} No changes-Incremental backup was not created" >> "${bc_lg_f}"
    fi
    # depict the inc bck time on std out
    echo "Check done on ${inc_bc_lg_ts}"

}

# same as above
if [ ! -d "${ib_d}" ]; then
    # inc dir is absent create it
    mkdir -p "${ib_d}"
    # this arg avoids err if already present
fi

# same as above for cmp bck
if [ ! -d "${cb_d}" ]; then
    mkdir -p "${cb_d}"
fi

# init ts with below
lst_cb_ts=""
cb_cnt=1
ib_cnt=0
# init ts for inc as below
lst_ib_ts=""

# Can change this to change timing
cur_slp_intr=30
while true; do
    # call fns accordingly
    
    cr_cmp_b
    sleep $cur_slp_intr

    cr_incre_b
    sleep $cur_slp_intr

    cr_incre_b
    sleep $cur_slp_intr

    cr_incre_b
    sleep $cur_slp_intr
done
